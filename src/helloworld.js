var express = require('express');
var port = 80;
var sim = express();
sim.get('/', function(req, res)
{
    res.send('Hello World from Nodejs and Docker!');
});

sim.get('/thunderheadeng', function(req, res)
{
	res.send('Simulation Software for Science and Engineering');
});

sim.listen(port);
console.log('Running nodejs on http://localhost:' + port);